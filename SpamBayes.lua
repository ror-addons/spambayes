--[[
	SpamBayes for Warhammer, a lightweight bayesian antispam for Warhammer Online
	Copyright � 2010 Arnaud Dovi (Merah@AthelLoren, ad@heapoverflow.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

--[[
$Rev:: 15                    $:  Revision of last commit
$Author:: merah              $:  Author of last commit
$Date:: 2010-12-10 19:32:58 #$:  Date of last commit
]]

----------------- GLOBALS ----------------

------------------------------------------

local d                               = d
local pairs                           = pairs
local ipairs                          = ipairs
local string_len                      = string.len
local string_lower                    = string.lower
local string_format                   = string.format
local tostring, towstring             = tostring, towstring
local TextLogAddEntry                 = TextLogAddEntry
local CreateHyperLink                 = CreateHyperLink
local GetChatChannelNames             = GetChatChannelNames
local TextLogAddFilterType            = TextLogAddFilterType
local GetFormatStringFromTable        = GetFormatStringFromTable
local LogDisplayGetFilterColor        = LogDisplayGetFilterColor
local LogDisplayGetFilterState        = LogDisplayGetFilterState
local LogDisplaySetFilterState        = LogDisplaySetFilterState

local DB, CDB, TEXT
local EADB                            = EA_ChatWindow.Settings
local EACOLORDB                       = ChatSettings.ChannelColors
local firstLoad                       = true
local isEventRegistered               = false
local SpamBayes                       = SpamBayes
local getprob                         = SpamBayes.chi2.getprob
local WarBoard_SpamBayes              = WarBoard_SpamBayes
local SPAMBAYES_BASE                  = 101000 -- must be unique, 750 already claimed by SpamMeNot
local MAX_WINDOW_GROUPS               = 10
local HARDSTRINGS                     = {}
local modded_buttons                  = {}
local clonedChatFilters               = { index = {}, struct = {} }
local orig_EA_ChatWindow              = {}
local orig_PlayerMenuWindow           = {}
local orig_EA_Window_ContextMenu      = {}
local orig_WindowUtils                = {}
local FILTER_UNSURE                   = 0
local FILTER_HAM                      = 1
local FILTER_SPAM                     = 2
local MAJOR_VERSION                   = "1.4.0"
local MINOR_VERSION                   = tonumber(("$Rev: 15 $"):match("%d+"))
SpamBayes.MINOR_VERSION               = SpamBayes.MINOR_VERSION == nil and MINOR_VERSION or (MINOR_VERSION > SpamBayes.MINOR_VERSION and MINOR_VERSION or SpamBayes.MINOR_VERSION)
-- 90% or greater   = SPAM
-- 20% or lower     = HAM
-- anything between = UNSURE
local SPAM_CUTOFF                     = 0.90
local HAM_CUTOFF                      = 0.20

local safeSystemChannels = { -- System filters misclassified as Chat by Mythic and not spammable...
	SystemData.ChatLogFilters.CITY_ANNOUNCE,
	SystemData.ChatLogFilters.CRAFTING,
	SystemData.ChatLogFilters.DEBUG,
	SystemData.ChatLogFilters.KILLS_DEATH_OTHER,
	SystemData.ChatLogFilters.KILLS_DEATH_YOURS,
	SystemData.ChatLogFilters.LOOT,
	SystemData.ChatLogFilters.LOOT_COIN,
	SystemData.ChatLogFilters.LOOT_ROLL,
	SystemData.ChatLogFilters.MISC,
	SystemData.ChatLogFilters.MONSTER_EMOTE,
	SystemData.ChatLogFilters.MONSTER_SAY,
	SystemData.ChatLogFilters.QUEST,
	SystemData.ChatLogFilters.RVR,
	SystemData.ChatLogFilters.TRADE,
	SystemData.ChatLogFilters.USER_ERROR,
	SystemData.ChatLogFilters.ZONE_AREA,
	-- removed the emote scans because due to a bad design of the Mythic client
	-- nor we can scan emote with custom text and there is no possible way to show builtin emotes
	-- nor we can show builtin emotes but there is no solution to scan custom emotes
	-- nor I do not scan emotes at all
	-- since you have to be near the spammer to be emote spammed, the third solution is the better
	-- Mythic Todo: add SystemData.ChatLogFilters.SYS_EMOTE for builtin emotes so
	-- SystemData.ChatLogFilters.EMOTE only refers to custom emotes!
	-- OR you throw emote informations to the chat event for builtin Emotes aswell, thank you!
	SystemData.ChatLogFilters.EMOTE,
	100, -- used by Mythic in debug.lua line 6
	751, -- used by SpamMeNot (anyway it sucks don't use it ;p)
	752, -- used by SpamMeNot
	753, -- used by SpamMeNot
	754  -- used by SpamMeNot
}

local hardcoded = {
	[SystemData.ChatLogFilters.ADVICE]           = "CHAT_TAG_ADVICE",            -- [Advice][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.ALLIANCE]         = "CHAT_TAG_ALLIANCE",          -- [Alliance][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.ALLIANCE_OFFICER] = "CHAT_TAG_ALLIANCE_OFFICER",  -- [Alliance Officer][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.BATTLEGROUP]      = "CHAT_TAG_BATTLEGROUP",       -- [Warband][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.CHANNEL_1]        = "CHAT_TAG_CHANNEL",           -- [<<3>>: <<4>>][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.CHANNEL_2]        = "CHAT_TAG_CHANNEL",           -- [<<3>>: <<4>>][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.CHANNEL_3]        = "CHAT_TAG_CHANNEL",           -- [<<3>>: <<4>>][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.CHANNEL_4]        = "CHAT_TAG_CHANNEL",           -- [<<3>>: <<4>>][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.CHANNEL_5]        = "CHAT_TAG_CHANNEL",           -- [<<3>>: <<4>>][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.CHANNEL_6]        = "CHAT_TAG_CHANNEL",           -- [<<3>>: <<4>>][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.CHANNEL_7]        = "CHAT_TAG_CHANNEL",           -- [<<3>>: <<4>>][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.CHANNEL_8]        = "CHAT_TAG_CHANNEL",           -- [<<3>>: <<4>>][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.CHANNEL_9]        = "CHAT_TAG_CHANNEL",           -- [<<3>>: <<4>>][<<1>>]: <<X:2>>
	--[SystemData.ChatLogFilters.EMOTE]            = "CHAT_TAG_EMOTE",             -- <<1>> <<X:2>>
	[SystemData.ChatLogFilters.GROUP]            = "CHAT_TAG_GROUP",             -- [Party][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.GUILD]            = "CHAT_TAG_GUILD",             -- [Guild][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.GUILD_OFFICER]    = "CHAT_TAG_GUILD_OFFICER",     -- [Officer][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.REALM_WAR_T1]     = "CHAT_TAG_REALM_WAR",         -- [Realm War Tier <<3>>][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.REALM_WAR_T2]     = "CHAT_TAG_REALM_WAR",         -- [Realm War Tier <<3>>][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.REALM_WAR_T3]     = "CHAT_TAG_REALM_WAR",         -- [Realm War Tier <<3>>][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.REALM_WAR_T4]     = "CHAT_TAG_REALM_WAR",         -- [Realm War Tier <<3>>][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.SAY]              = "CHAT_TAG_SAY",               -- [<<1>>] says: <<X:2>>
	[SystemData.ChatLogFilters.SCENARIO]         = "CHAT_TAG_SCENARIO",          -- [Scenario][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.SCENARIO_GROUPS]  = "CHAT_TAG_SCENARIO_GROUP",    -- [Scenario Party][<<1>>]: <<X:2>>
	[SystemData.ChatLogFilters.SHOUT]            = "CHAT_TAG_YELL",              -- <<1>> shouts: <<X:2>>
	[SystemData.ChatLogFilters.TELL_RECEIVE]     = "CHAT_TAG_TELL_RECIEVE",      -- [<<1>>] tells you: <<X:2>>
	[SystemData.ChatLogFilters.TELL_SEND]        = "CHAT_TAG_TELL_SEND"          -- You tell [<<1>>]: <<X:2>>
}

local tierNum   = {
	[SystemData.ChatLogFilters.REALM_WAR_T4] = 4,
	[SystemData.ChatLogFilters.REALM_WAR_T3] = 3,
	[SystemData.ChatLogFilters.REALM_WAR_T2] = 2,
	[SystemData.ChatLogFilters.REALM_WAR_T1] = 1
}

local isSafeCustomChannel = { -- put here channel names from addons like SoR
	["SoR1.3order"] = true, -- SoR
	["SoR1.3destr"] = true  -- SoR
}

local function LoadHardcodedTable() -- load, filter, unload, clean mem, is stabler and safer method than hardcoding numbers in codes, it now supports 1.3.6 & 1.4.0 clients unlike the harcoded method seen in spammenot
	LoadStringTable("HardcodedStrings", "data/strings/<LANG>", "hardcoded.txt", "cache/<LANG>/spambayes", "spambayestempglobal") -- the trick here is to load the table in a variable to get index numbers of your current client
	for tag, code in pairs(spambayestempglobal) do -- but this table is quite big so I extract the data we need in the local table, then clean junks from memory
		for _, tagb in pairs(hardcoded) do
			if tag == tagb then HARDSTRINGS[tag] = code end
		end
	end
	UnloadStringTable("HardcodedStrings")
	spambayestempglobal = nil
	collectgarbage("collect")
end

local function D(text)
	local eatab, sblog = "EA_ChatTab1TextLog", "SpamBayesDebug"
	TextLogClear(sblog)
	LogDisplaySetFilterColor(eatab, sblog, SPAMBAYES_BASE - 1, LogDisplayGetFilterColor(eatab, "Chat", SystemData.ChatLogFilters.MISC))
	TextLogAddEntry(sblog, SPAMBAYES_BASE - 1, L"[SpamBayes]: "..text)
end

local function InitDB()
	SpamBayes_DB              = SpamBayes_DB    or { isAddOnEnabled = true }
	DB                        = SpamBayes_DB
	DB.worddb                 = DB.worddb       or {}
	DB.classifierdb           = DB.classifierdb or { nham = 0, nspam = 0, cspam = 0, cham = 0, cunsure = 0, probcache = {}}
	CDB                       = DB.classifierdb
end

local function isAddOnEnabled()
	return DB["isAddOnEnabled"] == true
end

local function HookChatWindow()
	local orig_ChatWindow_OnKeyEnter = EA_ChatWindow.OnKeyEnter
	EA_ChatWindow.OnKeyEnter = function(...)
		local input = tostring(EA_TextEntryGroupEntryBoxTextInput.Text)
		local cmd, args = input:match("^/([a-zA-Z]+) ?(.*)")
		if cmd then
			if cmd:lower() == "spambayes" or cmd:lower() == "sb" then
				SpamBayes.ToggleControlCenter()
				EA_TextEntryGroupEntryBoxTextInput.Text = L""
			end
		end
		return orig_ChatWindow_OnKeyEnter(...)
	end
end

local function InitSlashCommands()
	if LibSlash then
		LibSlash.RegisterSlashCmd("spambayes", SpamBayes.ToggleControlCenter)
		LibSlash.RegisterSlashCmd("sb", SpamBayes.ToggleControlCenter)
	else
		HookChatWindow()
	end
end

local function updateCounters(train, class)
	if train == true then LabelSetText("SpamBayesWindowInfoTrainCounter",      towstring(string_format("Total messages trained: %d Spam, %d Ham", CDB.nspam, CDB.nham))) end
	if class == true then LabelSetText("SpamBayesWindowInfoTrainCounterClass", towstring(string_format("Messages classified: %d Spam, %d Ham, %d Unsure", CDB.cspam, CDB.cham, CDB.cunsure))) end
end

local function Initialize()
	local sep                = L"-"
	local eatab, sblog       = "EA_ChatTab1TextLog", "SpamBayesDebug"
	local addonLog           = "SpamBayesLog"
	local addonWindow        = "SpamBayesWindow"
	local button             = "Button"
	local separator          = "Separator"
	local addonOptionsWindow = "SpamBayesOptions"
	local addonHelpsWindow   = "SpamBayesHelps"
	local addonAboutWindow   = "SpamBayesAbout"
	local addonLogFrame      = addonWindow.."LogText"
	local addonFilterUnsure  = 0
	local addonFilterHam     = 1
	local addonFilterSpam    = 2
	local MINOR_VERSIONXML   = tonumber(WindowGetParent("SpamBayesRevisionText"):match("%d+")) -- hack to get the XML revision
	SpamBayes.MINOR_VERSION  = SpamBayes.MINOR_VERSION == nil and MINOR_VERSIONXML or (MINOR_VERSIONXML > SpamBayes.MINOR_VERSION and MINOR_VERSIONXML or SpamBayes.MINOR_VERSION)
	InitDB()
	InitSlashCommands()
	TextLogCreate(sblog, 1)
	TextLogAddFilterType(sblog, SPAMBAYES_BASE - 1, L"")
	LogDisplayAddLog(eatab, sblog, true)
	LabelSetText(addonWindow..separator.."1", sep)
	LabelSetText(addonWindow..separator.."2", sep)
	LabelSetText(addonWindow..separator.."3", sep)
	LabelSetText(addonAboutWindow.."BText", L"ABOUT")
	LabelSetText(addonAboutWindow.."Text1", L"SpamBayes for Warhammer")
	LabelSetText(addonAboutWindow.."Text1Version", L"v"..towstring(MAJOR_VERSION)..L" Build ")
	LabelSetText(addonAboutWindow.."Text1VersionRevision", towstring(SpamBayes.MINOR_VERSION))
	LabelSetText(addonAboutWindow.."Text2", L"by Mera, Merae, Merah @ Athel Loren FR")
	LabelSetText(addonAboutWindow.."Text3", L"- uses an algorithm of probabilities converted from SpamBayes Python a spam filter developed since 2002\nwhich is a combination of work from Gary Robinson and Tim Peters and many contributers\nsee http://spambayes.sourceforge.net/background.html.\n\n- Textures made with Paint.NET http://www.getpaint.net.")
	LabelSetText(addonAboutWindow.."Text4", L"(An integration of SpamBayes into Thunderbird3 exist, look for the plugin ThunderBayes++ I am maintaining for TB3)")
	LabelSetText(addonHelpsWindow.."BText", L"HELPS")
	LabelSetText(addonOptionsWindow.."BText", L"OPTIONS")
	LabelSetText(addonHelpsWindow.."T1", L"- By default, the database is blank, all messages received are classified Unsure, you will have to teach to distinguish spam from a ham.\n(support for an \"official\" and ready-to-use database is possible, see the suggestion line and follow the instructions).")
	LabelSetText(addonHelpsWindow.."T1T2", L"- Learning a chat message as ham or spam is done by right-clicking the playername in the chatwindow or this control center.")
	LabelSetText(addonHelpsWindow.."T1T2T3", L"- It is better to keep a balanced number of messages learned or a number of ham slightly higher in preferring to teach him phrases containing the most words and hams identified greater than 50%.")
	LabelSetText(addonHelpsWindow.."T1T2T3T4", L"- Messages displayed in Red = Spam, Green = Ham, and Blank = Unsure. Unlike most antispams, SpamBayes never totally hides a spam, it displays everything in the control center and the chat windows except spams (by default, messages higher than 90% probability).")
	LabelSetText(addonHelpsWindow.."T1T2T3T4T5", L"- Unlike most antispams, SpamBayes is very intelligent, it does not uses hardcoded string patterns of known spammers, you are not forced to download X update of the AddOn to stop Y spammer, but instead, it uses a powerful algorithm designed by a vaste community of geeks to calculate the weight of a phrase as a probability so by training X and Y message, you will be able to stop A, B, C messages.")
	LabelSetText(addonHelpsWindow.."T1T2T3T4T5T6", L"- The more you will train unique spams and hams and the more autonomous it will be and the more correct classification will be.")
	LabelSetText(addonHelpsWindow.."T1T2T3T4T5T6T7", L"- The settings and the spam database are global to all characters and are stored in the folder user\\settings\\GLOBAL\\SpamBayes")
	LabelSetText(addonHelpsWindow.."T1T2T3T4T5T6T7T8", L"- Automatic reports are not yet implemented but are possible.")
	LabelSetText(addonHelpsWindow.."T1T2T3T4T5T6T7T8T9", L"- BUGS & SUGGESTIONS: must be sent to Curseforge ticket, if the report already exists vote and comment inside, if not, create a new ticket. Any bug or suggestion made outside this rule will not be considered.")
	LabelSetText(addonWindow.."TitleText", L"SpamBayes Control Center")
	ButtonSetText(addonWindow.."Clear"..button, L"Clear")
	ButtonSetText(addonWindow.."Options"..button, L"Options")
	ButtonSetText(addonWindow.."About"..button, L"About")
	ButtonSetText(addonWindow.."Help"..button, L"Help")
	LabelSetText(addonOptionsWindow.."IsAddOnEnabledLabel", L"SpamBayes enabled")
	ButtonSetPressedFlag(addonOptionsWindow.."IsAddOnEnabled"..button, isAddOnEnabled())
	ButtonSetCheckButtonFlag(addonOptionsWindow.."IsAddOnEnabled"..button, isAddOnEnabled())
	WindowSetShowing(addonOptionsWindow, false)
	WindowSetShowing("SpamBayesHelps", false)
	WindowSetShowing("SpamBayesAbout", false)
	updateCounters(true, true)

	TextLogCreate(addonLog, 10000)
	--TextLogClear(addonLog)
	TextLogSetIncrementalSaving(addonLog, true, L"logs/spambayes.log")
	TextLogAddFilterType(addonLog, 0, L"")
	LogDisplayAddLog(addonLogFrame, addonLog, true)
	LogDisplaySetShowTimestamp(addonLogFrame, true)
	LogDisplaySetShowLogName(addonLogFrame, false)
	TextLogDisplayShowScrollbar(addonLogFrame, true)
	LogDisplaySetFilterColor(addonLogFrame, addonLog, FILTER_UNSURE, 255, 255, 255)
	LogDisplaySetFilterColor(addonLogFrame, addonLog, FILTER_HAM, 0, 255, 0)
	LogDisplaySetFilterColor(addonLogFrame, addonLog, FILTER_SPAM, 255, 0, 0)
	--TextLogAddEntry(addonLog, 0, towstring(addonLog)..L" activated")

	d("SpamBayes for Warhammer initialized.")
	D(isAddOnEnabled() and L"ON" or L"OFF")
end

local function isTrueChatChannel(id)
	for _, systemId in pairs(safeSystemChannels) do
		if id == systemId then return false end
	end
	return true
end

local function CloneChatFilters() -- Caching
	clonedChatFilters = { index = {}, struct = {} }
	local eatab = ChatSettings.Channels
	for group = 1, MAX_WINDOW_GROUPS do
		if EA_ChatWindowGroups[group].used then
			clonedChatFilters.struct[group] = {}
			local groupTabs = EA_ChatWindowGroups[group].Tabs
			for tabId, _ in ipairs(groupTabs) do
				clonedChatFilters.struct[group][tabId] = {}
				for channelId, channelData in pairs(eatab) do
					if channelData.logName == "Chat" and isTrueChatChannel(channelId) and channelData.isForSpamBayes ~= true then
						if not clonedChatFilters.struct[group][tabId][channelId] then
							local newId = SPAMBAYES_BASE + channelId
							TextLogAddFilterType("Chat", newId, L"")
							clonedChatFilters.struct[group][tabId][channelId] = 1
							clonedChatFilters.index[channelId] = newId
						end
					end
				end
			end
		end
	end
	--d(clonedChatFilters.index)
end

local function ToggleCustomChatFilters(enable) -- Cloning
	local hookChat = enable == true
	for group, groupData in pairs(clonedChatFilters.struct) do
		for tabId, tabData in ipairs(groupData) do
			local tabName = EA_ChatTabManager.Tabs[tabId].name.."TextLog"
			for realId, _ in pairs(tabData) do
				local newId = clonedChatFilters.index[realId]
				local isNew = type(EADB.WindowGroups[group].Tabs[tabId].Filters[newId]) == "nil"
				local colorNew = type(EACOLORDB[newId]) == "nil"
				local flag, color
				if isNew then
					flag = LogDisplayGetFilterState(tabName, "Chat", hookChat and realId or newId)
				else
					flag = EADB.WindowGroups[group].Tabs[tabId].Filters[newId]
				end
				if colorNew then
					color = { LogDisplayGetFilterColor(tabName, "Chat", hookChat and realId or newId) }
				else
					color = { EACOLORDB[newId].r, EACOLORDB[newId].g, EACOLORDB[newId].b }
				end
				LogDisplaySetFilterState(tabName, "Chat", hookChat and newId or realId, flag) -- toggle on/off real or fake filters
				LogDisplaySetFilterState(tabName, "Chat", hookChat and realId or newId, false) -- force off real or fake filters
				LogDisplaySetFilterColor(tabName, "Chat", hookChat and newId or realId, color[#color - 2], color[#color - 1], color[#color]) -- apply colors
				EACOLORDB[hookChat and newId or realId]   = EACOLORDB[hookChat and newId or realId] or {}
				EACOLORDB[hookChat and newId or realId].r = color[#color - 2]
				EACOLORDB[hookChat and newId or realId].g = color[#color - 1]
				EACOLORDB[hookChat and newId or realId].b = color[#color]
			end
		end
	end
	--d(clonedChatFilters)
end

local function ToggleCustomChatConfig(enable) -- Setting
	local odrTable   = ChatSettings.Ordering
	local chltable   = ChatSettings.Channels
	local swTable    = ChatSettings.ChannelSwitches
	local clrTable   = ChatSettings.ChannelColors
	local copyTable  = DataUtils.CopyTable
	local enabled = enable == true
	for a, b in pairs(clonedChatFilters.index) do
		chltable[b] = enabled and copyTable(chltable[a]) or nil
		if chltable[b] ~= nil then
			chltable[b].id = b
			chltable[b].isOn = enabled
			chltable[b].isForSpamBayes = true -- secure check
		end
		swTable[b] = enabled and { commands= L"", replacement = L"" } or nil
		local color = clrTable[a].r ~= nil and copyTable(clrTable[a]) or nil
		clrTable[b] = enabled and color or nil
	end
	for e, f in pairs(odrTable) do
		if enabled then
			if type(clonedChatFilters.index[f]) == "number" then
				odrTable[e] = clonedChatFilters.index[f]
			end
		else
			for realid, newid in pairs(clonedChatFilters.index) do
				if newid == f then
					odrTable[e] = realid
				end
			end
		end
	end
end

local function HideClonedChannelsOnReloadUI()
	local eatab = ChatSettings.Channels
	for group = 1, MAX_WINDOW_GROUPS do
		if EA_ChatWindowGroups[group].used then
			local groupTabs = EA_ChatWindowGroups[group].Tabs
			for tabId, _ in ipairs(groupTabs) do
				local tabName = EA_ChatTabManager.Tabs[tabId].name.."TextLog"
				for channelId, channelData in pairs(eatab) do
					if channelData.logName == "Chat" and isTrueChatChannel(channelId) then
						local newId = SPAMBAYES_BASE + channelId
						LogDisplaySetFilterState(tabName, "Chat", newId, false)
					end
				end
			end
		end
	end
end

local function string_split(text, sep, pattern)
	local sep, fields = sep or " ", {}
	local patt = pattern or string_format("([^%s]+)", sep)
	text:gsub(patt, function(c) fields[#fields+1] = string_lower(c) end)
	return fields
end

local function tokenize(msg)
	return string_split(tostring(msg), _, "([^%s%p%c]+)") -- obfuscations removal
end

local function SpamBayesTrain(isSpam, unLearn)
	-- when LogDisplayGetStringFromCursorPos() returns nothing you get the "Unexpected error", this is a client weakness, retry unlearning in this case
	local tokens = TEXT ~= nil and tokenize(TEXT) or D(L"Unexpected error while "..(unLearn and L"unlearning" or L"training")..L" as "..(isSpam and L"SPAM" or L"HAM")..L" (please retry this is a known client bug")
	TEXT = nil
	--d(tokens)
	if type(tokens) == "table" then
		local success
		if unLearn then
			success = SpamBayes.chi2.unlearn(tokens, isSpam)
		else
			SpamBayes.chi2.learn(tokens, isSpam)
		end
		D((success == 0 and L"Error with " or L"Success with ")..(unLearn and L"unlearn" or L"train")..L" one more "..(isSpam and L"SPAM" or L"HAM")..(success == 0 and L" (the phrase was never learned as "..(isSpam and L"spam" or L"ham")..L" or slightly changed)" or L""))
		updateCounters(true, false)
	end
end

local buttons = {
	[0] = { L"SpamBayes", nil, 205, 204, 102 },
	[1] = { L"train Ham", function() SpamBayesTrain(false) end, 0, 255, 0 },
	[2] = { L"train Spam", function() SpamBayesTrain(true) end, 255, 0, 0 },
	[3] = { L"unlearn Ham", function() SpamBayesTrain(false, true) end, 128, 128, 128 },
	[4] = { L"unlearn Spam", function() SpamBayesTrain(true, true) end, 128, 128, 128 }
}

local function changeButtonColor(buttonText, contextMenuNumber)
	if type(buttonText) == "boolean" then
		for a in pairs(modded_buttons) do
			ButtonSetTextColor(a, 0, 255, 255, 255)
			modded_buttons[a] = nil
		end
		return
	end
	for i = 0, #buttons do
		if buttonText == buttons[i][1] then
			local menuTable        = EA_Window_ContextMenu.contextMenus[contextMenuNumber]
			local frame            = menuTable.anchorWindow
			modded_buttons[frame]  = ""
			ButtonSetTextColor(frame, 0, buttons[i][3], buttons[i][4], buttons[i][5])
		end
	end
end

local function HookAddMenuItem(buttonText, _, __, ___, contextMenuNumber, ...)
	if buttonText == buttons[0][1] then
		EA_Window_ContextMenu.AddMenuDivider(contextMenuNumber)
	end
	orig_EA_Window_ContextMenu.AddMenuItem(buttonText, _, buttonText == buttons[0][1] and true or __, ___, contextMenuNumber, ...)
	changeButtonColor(buttonText, contextMenuNumber)
end

local function HookShowMenu(_, __, items, ...)
	EA_Window_ContextMenu.AddMenuItem = HookAddMenuItem
	-- for security, unlearn will break if one token is not in our db because this should only be used to correct mistakes in recent classifications
	for i = 0, #buttons do
		table.insert(items, PlayerMenuWindow.NewCustomItem(buttons[i][1], buttons[i][2]))
	end
	orig_PlayerMenuWindow.ShowMenu(_, __, items, ...)
end

local function HookHideMenu(...)
	changeButtonColor(true)
	orig_WindowUtils.OnHidden(...)
	if orig_WindowUtils.OnHidden then WindowUtils.OnHidden = orig_WindowUtils.OnHidden end
end

local function HookOnPlayerLinkRButtonUp(_, __, x, y, wndGroupId, ...)
	local activeTabName
	PlayerMenuWindow.ShowMenu = HookShowMenu
	WindowUtils.OnHidden      = HookHideMenu
	if wndGroupId ~= nil and wndGroupId ~= 0 then
		local wndGroup = EA_ChatWindowGroups[wndGroupId]
		local activeTabId = wndGroup.activeTab
		activeTabName = EA_ChatTabManager.GetTabName(wndGroup.Tabs[activeTabId].tabManagerId)
	end
	local line = LogDisplayGetStringFromCursorPos((wndGroupId ~= nil and wndGroupId ~= 0) and activeTabName.."TextLog" or "SpamBayesWindowLogText", x, y) -- you can learn from the Chat or the control center
	TEXT = line:match(L"^.*[\x0E](.*)") -- cool magic bit indeed ?:)
	--d(TEXT)
	orig_EA_ChatWindow.OnPlayerLinkRButtonUp(_, __, x, y, wndGroupId, ...)
	if orig_PlayerMenuWindow.ShowMenu then PlayerMenuWindow.ShowMenu = orig_PlayerMenuWindow.ShowMenu end
	if orig_EA_Window_ContextMenu.AddMenuItem then EA_Window_ContextMenu.AddMenuItem = orig_EA_Window_ContextMenu.AddMenuItem end
end

local function Install_or_Remove_Hooks(forceOff)
	if not forceOff and isAddOnEnabled() then
		if not isEventRegistered then
			LoadHardcodedTable()
			CloneChatFilters()
			ToggleCustomChatFilters(true)
			ToggleCustomChatConfig(true)
			EA_ChatWindow.settingsDirty = true
			EA_ChatWindow.SaveSettings()
			RegisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED, "SpamBayes.OnChatTextArrived")
			isEventRegistered = true
		end
		EA_ChatWindow.OnPlayerLinkRButtonUp  = HookOnPlayerLinkRButtonUp
		EA_ChatWindow.Shutdown               = SpamBayes.Shutdown
	else
		if isEventRegistered then
			ToggleCustomChatFilters(false)
			ToggleCustomChatConfig(false)
			EA_ChatWindow.settingsDirty = true
			EA_ChatWindow.SaveSettings()
			UnregisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED, "SpamBayes.OnChatTextArrived")
			isEventRegistered = false
		end
		HideClonedChannelsOnReloadUI() -- not needed but cleaner
		if orig_EA_ChatWindow.OnPlayerLinkRButtonUp then EA_ChatWindow.OnPlayerLinkRButtonUp = orig_EA_ChatWindow.OnPlayerLinkRButtonUp end
		if orig_EA_ChatWindow.Shutdown then EA_ChatWindow.Shutdown = orig_EA_ChatWindow.Shutdown end
	end
end

function SpamBayes.UpdateButtonIsAddOnEnabled()
	local buttonName = SystemData.ActiveWindow.name.."Button"
	if (ButtonGetDisabledFlag(buttonName)) then return end
	local pressed = ButtonGetPressedFlag(buttonName)
	ButtonSetPressedFlag(buttonName, not pressed)
	DB["isAddOnEnabled"] = not pressed
	Install_or_Remove_Hooks(false)
	D(isAddOnEnabled() and L"ON" or L"OFF")
end

function SpamBayes.BackupOriginalFunctions()
	if not firstLoad then return end
	firstLoad = false
	orig_EA_ChatWindow.OnPlayerLinkRButtonUp = EA_ChatWindow.OnPlayerLinkRButtonUp
	orig_EA_ChatWindow.Shutdown              = EA_ChatWindow.Shutdown
	orig_PlayerMenuWindow.ShowMenu           = PlayerMenuWindow.ShowMenu
	orig_WindowUtils.OnHidden                = WindowUtils.OnHidden
	orig_EA_Window_ContextMenu.AddMenuItem   = EA_Window_ContextMenu.AddMenuItem
	Initialize()
	Install_or_Remove_Hooks(false)
end

function SpamBayes.ToggleControlCenter()
	WindowSetShowing("SpamBayesWindow", not WindowGetShowing("SpamBayesWindow"))
	if not WindowGetShowing("SpamBayesWindow") then
		WindowSetShowing("SpamBayesOptions", false)
		WindowSetShowing("SpamBayesHelps", false)
		WindowSetShowing("SpamBayesAbout", false)
	end
end

function SpamBayes.ToggleOptions()
	if not WindowGetShowing("SpamBayesWindow") then
		WindowSetShowing("SpamBayesWindow", true)
	end
	if WindowGetShowing("SpamBayesHelps") then
		WindowSetShowing("SpamBayesHelps", false)
	end
	if WindowGetShowing("SpamBayesAbout") then
		WindowSetShowing("SpamBayesAbout", false)
	end
	WindowSetShowing("SpamBayesOptions", not WindowGetShowing("SpamBayesOptions"))
end

function SpamBayes.ToggleHelps()
	if not WindowGetShowing("SpamBayesWindow") then
		WindowSetShowing("SpamBayesWindow", true)
	end
	if WindowGetShowing("SpamBayesOptions") then
		WindowSetShowing("SpamBayesOptions", false)
	end
	if WindowGetShowing("SpamBayesAbout") then
		WindowSetShowing("SpamBayesAbout", false)
	end
	WindowSetShowing("SpamBayesHelps", not WindowGetShowing("SpamBayesHelps"))
end

function SpamBayes.ToggleAbout()
	if not WindowGetShowing("SpamBayesWindow") then
		WindowSetShowing("SpamBayesWindow", true)
	end
	if WindowGetShowing("SpamBayesOptions") then
		WindowSetShowing("SpamBayesOptions", false)
	end
	if WindowGetShowing("SpamBayesHelps") then
		WindowSetShowing("SpamBayesHelps", false)
	end
	WindowSetShowing("SpamBayesAbout", not WindowGetShowing("SpamBayesAbout"))
end

function SpamBayes.OnResize(...)
    WindowUtils.BeginResize( "SpamBayesWindow", "topleft", 300, 200, nil)
end

function SpamBayes.ClearLog()
    TextLogClear("SpamBayesLog")
end

function SpamBayes.OnChatTextArrived()
	local id        = GameData.ChatData.type
	local matrix    = hardcoded[id] and HARDSTRINGS[hardcoded[id]] or nil
	if matrix == nil then return end
	local prob, tname
	local isChannel = matrix == HARDSTRINGS.CHAT_TAG_CHANNEL
	local isRealm   = matrix == HARDSTRINGS.CHAT_TAG_REALM_WAR
	local channels  = isChannel and GetChatChannelNames() or nil
	local isSafe    = isChannel and isSafeCustomChannel[tostring(channels[id].name)] == true or false
	local sysTable  = SystemData.ChatLogFilters
	local text      = GameData.ChatData.text
	local who       = GameData.ChatData.name
	local whotable  = {
		who2link    = function() return who ~= GameData.Player.name and CreateHyperLink(L"PLAYER:"..towstring(who), towstring(who), {LogDisplayGetFilterColor("EA_ChatTab1TextLog", "Chat", id)}, {}) or who end,
		who2linkh   = function() return who ~= GameData.Player.name and CreateHyperLink(L"PLAYER:"..towstring(who), towstring(who), {0, 255, 0}, {}) or who end,
		who2links   = function() return who ~= GameData.Player.name and CreateHyperLink(L"PLAYER:"..towstring(who), towstring(who), {255, 0, 0}, {}) or who end,
		who2linku   = function() return who ~= GameData.Player.name and CreateHyperLink(L"PLAYER:"..towstring(who), towstring(who), {255, 255, 255}, {}) or who end
	}
	for fname, fid in pairs(sysTable) do
		if fid == id then tname = fname break end
	end
	text             = L"\x0E"..text -- 0x0E is used to identify the beginning of strings safe to tokenize and learn, SpamBayes must not train part of strings beginning with [1: Region] etc...
	local str        = GetFormatStringFromTable("Hardcoded", matrix, isChannel and {whotable["who2link"](), text, channels[id].number, tostring(channels[id].name)} or isRealm and {whotable["who2link"](), text, tierNum[id]} or {whotable["who2link"](), text})
	if not isSafe then
		prob         = getprob(tokenize(text))
		local prob_l = string_format("%.1f", prob * 100)
		local prob_t = string_format(string_len(prob_l) == 4 and "[%s %%]" or ((string_len(prob_l) == 5 and prob_l == "100.0") and "[ 100 %%]" or "[0%s %%]"), prob_l)
		local strd   = GetFormatStringFromTable("Hardcoded", matrix, isChannel and {whotable[(prob >= SPAM_CUTOFF) and "who2links" or ((prob <= HAM_CUTOFF) and "who2linkh" or ("who2linku"))](), text, channels[id].number, tostring(channels[id].name)} or isRealm and {whotable[(prob >= SPAM_CUTOFF) and "who2links" or ((prob <= HAM_CUTOFF) and "who2linkh" or ("who2linku"))](), text, tierNum[id]} or {whotable[(prob >= SPAM_CUTOFF) and "who2links" or ((prob <= HAM_CUTOFF) and "who2linkh" or ("who2linku"))](), text})
		local isHam  = prob <= HAM_CUTOFF
		local isSpam = prob >= SPAM_CUTOFF
		-- display to SpamBayesLog
		TextLogAddEntry("SpamBayesLog", isHam and FILTER_HAM or (isSpam and FILTER_SPAM or FILTER_UNSURE), towstring(prob_t)..strd)
		if isHam then
			CDB.cham    = CDB.cham + 1
		elseif isSpam then
			CDB.cspam   = CDB.cspam + 1
		else
			CDB.cunsure = CDB.cunsure + 1
		end
		updateCounters(false, true)
	end
	-- and to cloned filters if spam_cutoff is not reached
	if (prob and prob < SPAM_CUTOFF) or isSafe then
		TextLogAddEntry("Chat", SPAMBAYES_BASE + id, str)
	end
end

function WarBoard_SpamBayes.Initialize()
	if WarBoard and WarBoard.AddMod("WarBoard_SpamBayes") then
		d(L"WarBoard_SpamBayes initialized")
	end
end

function SpamBayes.Shutdown()
	Install_or_Remove_Hooks(true)
	firstLoad = true
	orig_EA_ChatWindow.Shutdown()
end

RegisterEventHandler(SystemData.Events.LOADING_END, "SpamBayes.BackupOriginalFunctions")
RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "SpamBayes.BackupOriginalFunctions")
