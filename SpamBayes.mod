<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
	<UiMod name="SpamBayes" version="1.4.0" date="21/08/2010" >
		<Author name="Mera[he]- Order AthelLoren" email="ad@heapoverflow.com" />
		<Description text="A lightweight bayesian antispam for Warhammer Online" />
		<VersionSettings gameVersion="1.4.0" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies />
		<Files>
			<File name="chi2.lua" />
			<File name="SpamBayes.xml" />
		</Files>
		<SavedVariables>
			<SavedVariable name="SpamBayes_DB" global="true" />
		</SavedVariables>
		<OnInitialize>
			<CallFunction name="WarBoard_SpamBayes.Initialize" />
			<CreateWindow name="SpamBayesRevision"  show="false" />
			<CreateWindow name="SpamBayesWindow"    show="false" />
			<CreateWindow name="SpamBayesOptions"   show="false" />
			<CreateWindow name="SpamBayesHelps"     show="false" />
			<CreateWindow name="SpamBayesAbout"     show="false" />
		</OnInitialize>
	</UiMod>
</ModuleFile>